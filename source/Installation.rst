Prerequisites
=============

This tutorial assumes Linux (Ubuntu/Debian). You need ``python3`` installed in your system.


Sphinx 
------

The Sphinx documentation generation allows creating documentation in .html or .pdf formats from .rst text files. To install it:

>>> apt install python3-sphinx python3-pip

The package ``python3-pip`` is useful to install on-the-fly extensions and html themes for Sphinx.


Extensions for Sphinx
^^^^^^^^^^^^^^^^^^^^^

This step is optional and what you install here will not be used by default when compiling Sphinx projects unless specified in the conf.py file.

The most popular html theme for Sphinx `Read-the-Docs theme <https://sphinx-rtd-theme.readthedocs.io/en/stable/>`_:

>>> pip install sphinx_rtd_theme

To enable it you will have to edit the :ref:`html_theme line <html_theme>` in the conf.fy file once the project is created. There exist `other themes that are installed by default <https://www.sphinx-doc.org/en/master/usage/theming.html>`_.

Extensions can also be installed via `pip`, such as `sphinx-panels <https://sphinx-panels.readthedocs.io/>`_. This extension offers the possibility to layout panels, cards, images, badges, dropdown menus, animations...

>>> pip install sphinx-panels

Once installed, it should be enabled in the conf.fy file as well, adding it to the :ref:`extensions list <Extensions>`.

`sphinx-tabs <https://sphinx-tabs.readthedocs.io>`_ is a similar extension that allows nested tabs and synchronization between tabs, however, its implementation in LaTeX is not the best.


LaTeX
-----

If you want to compile the documentation in LaTeX, you need the following packages installed:

>>> apt install texlive texlive-science texlive-pictures latexmk


Git
---

To upload the code and files generated to a repository (e.g. Gitlab), you need ``Git`` installed in your system.

>>> apt install git


VS-Code and useful plugins
--------------------------

VS-Code is an IDE that allows to easily install plugins that are useful for working with ``.rst`` files and quickly create code documentation. VS-Code can be installed through Snapd:

>>> snap install code --classic

You can open this IDE in the console with:

>>> code

The following extensions are useful when working with ``.rst`` files in Sphinx documentation:

.. image:: ./images/vscode-extensions.png
    :align: center

They can be installed by clicking on the left menu :guilabel:`Extensions` and look for them in the marketplace. reStructuredText and reStructuredText Syntax highlighting are useful for previewing the generated html on the fly (files stored in the **_build** directory inside the **/source folder**!!) and autocompletion of Sphinx directives, while Python Docstring generator allows to quickly generate function documentation that of python code, that can be read and parsed by Sphinx. Finally, the LTeX extension servers as a grammar/spell checker in English when writing in reStructuredText, but also in LaTeX and other languages.

