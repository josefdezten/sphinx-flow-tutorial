Flow with Git
=============

You can use Git to keep track of the changes in the project (or any software project), and as a webserver to deploy the html files.


Preparing the Sphinx project
----------------------------

The ``source`` folder is the most important one to save in the repository, **particularly the conf.py and .rst files**, as the html or pdf outputs can be compiled with those. The 'Git tracks changes of files within your project, and folders with files. However, it doesn't track empty folders. There are two folders. It is convenient to explicitly add the ``source/_static`` and ``source/_templates`` to the git repository, despite these folders are empty. Otherwise, if you or another person clones the repository locally and compiles a html, compilation warnings will be shown in the process because these folders would be missing. For this, you just have to create a dummy file, called for example ``.gitkeep`` inside these folders (only once) to make git track them as well:

>>> touch source/_static/.gitkeep
>>> touch source/_templates/.gitkeep

It is also good practice avoiding files that are not useful for the project, such as temporary files or files created by others programs (e.g. VS-Code), as they would just fill with trash the repository, and you will most likely get conflicts when pushing/pulling the project from useless files, particularly if working with other people. To tell git what files or folders in the project should be ignored, you can create a file in the top project folder named ``.gitignore``:  

>>> touch .gitignore

Open it with any text editor, and just add paths to places to be ignored, such as the ``source/_build`` directory (note that it is the one inside source), that is the folder where VS-Code stored the html that are generated previsualization (in case you use this feature), or the ``.vscode`` folder that is always created wherever you open this IDE.

.. code-block:: none

    source/_build/
    source/.vscode/

Depending on personal preferences, you may add to gitignore the entire ``build`` folder as it is just the compilation output and you may deploy the webpage in a different place. On top of that, you can add entire file extensions to gitignore as \*.extension.


New Gitlab account (only first time)
------------------------------------

If you don't have an account, just go to www.gitlab.com to create one. Then, go to your terminal and list any the git users active in your session:

>>> git config --list

If you don't show up there, add yourself to the session (only to be done once):

>>> git config --global user.name "Your name"
>>> git config --global user.email "your@email.com"

In this way anytime you push something to the repository, other people will be able to see who did what.


Pushing an existing folder to a new Gitlab repository
-----------------------------------------------------

Before pushing the project to the online repository, you need to create a Gitlab project. After that, you will be provided a link to be added with the git-remote command when setting up the local repository:

>>> git init
>>> git remote add origin git@gitlab.com:user/link-to-repository.git
>>> git add <files>
>>> git commit -m "First commit"
>>> git push -u origin master

.. warning::
    In this new decadent era of censorship where everyone is offended by everything, Gitlab is pushing to change the name of the `master` branch to `main` because the former `is not inclusive and its offensive because of its history <https://about.gitlab.com/blog/2021/03/10/new-git-default-branch-name/>`_ (?). If you get errors when pushing your files, check with ``git status`` that your main/master branch is named locally in the same way as in the Gitlab repository (go to the project webpage and check the branch names). 

An alternative way to synchronize the local and the online repository avoiding the previous problem, is to create an empty project in Gitlab.com, and ``git clone`` it to an **empty folder** in your computer. Once you have done that, move your project files to this new local folder and execute the usual git add, git commit, git push routine.


Deploying the webpage with Gitlab Pages
---------------------------------------

You may want to make upload your webpage for other people to see the documentation that you have generated. For this, Gitlab Pages is one option that we can take advantage of since our project repository is already there.

There are two approaches to make the webpage visible depending on our personal preference. Either we create a new Gitlab repository to only upload the webpage and deploy it, or we upload the entire Sphinx project and deploy the build directory.

In order to deploy the web content, you will have to create a gitlab script in a file named ``.gitlab-ci.yml`` in the top folder. The code in this script will be then continuously launched by gitlab every time there is a change in the repository. Essentially, it will take the html files that you place in the top folder of the repository, and expose them to the Internet.

.. note::
    When deploying the html files for the first time with Gitlab Pages, you may be asked to validate your account by entering a payment method (you will be charged a small amount that will then be reimbursed).


As a separate project
^^^^^^^^^^^^^^^^^^^^^

This method is convenient if you want to isolate the Sphinx project from the generated documentation. This can be useful to keep cleaner repositories, get fewer warnings when performing ``git status`` (specially if you are working with someone else) and avoid updating the webpage everytime you make a change in the repository. On the downside, you will need to update two repositories: the sphinx sources on one hand, and the website with the htmls on the other. 

To start, you have to create a new Gitlab.com repository and a new git local repository. Then, move the content of your ``build/html`` folder in your Sphinx project, to this new local repository. Add files, commit and push the new repository as usual.

At this point go to your new gitlab.com repository and in the top folder, create a file named ``.gitlab-ci.yml`` with the following content:

.. code-block:: none

    pages:
    stage: deploy
    script:
        - mkdir .public
        - cp -r * .public
        - rm -rf public
        - mv .public public
    artifacts:
        paths:
          - public
    rules:
        - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

You can do it manually, or you can press :guilabel:`Add CONTRIBUTING` in the main gitlab project page:

.. image:: images/create_yml1.png

And then, under the tab :guilabel:`Select a template type` choose ``gitlab-ci.yml``, and under :guilabel:`Apply a template` choose ``HTML``. The text box will be automatically filled with the script lines as above. After that, commit changes.

.. image:: images/create_yml2.png

At this point, the script should be launched. Go to :guilabel:`CI/CD->Pipelines` in the left menu, and check the status of the deployment:

.. image:: images/pipelines.png

Check :guilabel:`Status` or press on it to check the running details. Bear in mind that the process can be fast or take some time to finish. If your status is ``passed``, the webpage is ready to be accessed.

To know the hyperlink to your webpage, you can go to :guilabel:`Settings->Pages` in the left menu. There, you will see the link under your webpage is served. 

.. image:: images/pages.png


.. note::
    If you have created your project with internal visibility level, only the people that you give access to will be able to see your webpage!


All in one project
^^^^^^^^^^^^^^^^^^

If you prefer to directly upload the webpage every time you push your local sphinx repository, you can modify the ``gitlab-ci.yml`` with the following code. This modification tells gitlab to expose the content of the ``build/html`` folder to the Internet:

.. code-block:: none
    :emphasize-lines: 7

    pages:
        stage: deploy
        script:
            - mkdir .public
            - cp -r build/html/. .public
            - mkdir .public/latex
            - cp build/latex/your-sphinx-project.pdf .public/latex/.
            - rm -rf public
            - mv .public public
        artifacts:
            paths:
            - public
        rules:
            - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

.. note::
    This piece of code contains two additional lines to publish the generated pdf. Make sure that you replace ``your-sphinx-project.pdf`` by the name of your pdf.

The following steps are exactly the same as explained in the previous section.




