.. sphinx-flow-tutorial documentation master file, created by
   sphinx-quickstart on Mon Dec 20 15:58:36 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to sphinx-flow-tutorial's documentation!
================================================

This short tutorial tries to explain the necessary steps from the installation of useful tools to the documentation generation with Sphinx in html and pdf formats, using git (Gitlab) as a repository to upload the created web content.

.. only:: html

   The documentation of this webpage is also available in pdf format `here <./latex/sphinx-flow-tutorial.pdf>`_.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   Installation
   CreatingSphinxProject
   flowWithGit

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
