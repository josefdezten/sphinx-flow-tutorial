Creating a Sphinx project
=========================

.. tip::
    Before reading this document, it is recommended to watch `this youtube video <https://www.youtube.com/watch?v=b4iFyrLQQh4>`_ (less than 5 mins!) to gain a general gist of the workflow with Sphinx.

    The long manual with everything you can do with Sphinx is in the following link:

    https://www.sphinx-doc.org/_/downloads/en/master/pdf/ 

sphinx-quickstart
-----------------

Create a new folder for your project and move inside, then execute:

>>> sphinx-quickstart

You will be presented with a questionnaire.

.. code-block:: none
    :emphasize-lines: 11,16,19,20,21,29,33,39,42,44,45,46,47,48,49,50,51,52,53,58,59

    Welcome to the Sphinx 1.7.1 quickstart utility.

    Please enter values for the following settings (just press Enter to
    accept a default value, if one is given in brackets).

    Selected root path: .

    You have two options for placing the build directory for Sphinx output.
    Either, you use a directory "_build" within the root path, or you separate
    "source" and "build" directories within the root path.
    > Separate source and build directories (y/n) [n]: y

    Inside the root directory, two more directories will be created; "_templates"
    for custom HTML templates and "_static" for custom stylesheets and other static
    files. You can enter another prefix (such as ".") to replace the underscore.
    > Name prefix for templates and static dir [_]: (ENTER)

    The project name will occur in several places in the built documentation.
    > Project name: yourproject
    > Author name(s): yourname
    > Project release []: 0.0.1

    If the documents are to be written in a language other than English,
    you can select a language here by its language code. Sphinx will then
    translate text that it generates into that language.

    For a list of supported codes, see
    http://sphinx-doc.org/config.html#confval-language.
    > Project language [en]: (ENTER)

    The file name suffix for source files. Commonly, this is either ".txt"
    or ".rst".  Only files with this suffix are considered documents.
    > Source file suffix [.rst]: (ENTER)

    One document is special in that it is considered the top node of the
    "contents tree", that is, it is the root of the hierarchical structure
    of the documents. Normally, this is "index", but if your "index"
    document is a custom template, you can also set this to another filename.
    > Name of your master document (without suffix) [index]: (ENTER)

    Sphinx can also add configuration for epub output:
    > Do you want to use the epub builder (y/n) [n]: (ENTER)
    Indicate which of the following Sphinx extensions should be enabled:
    > autodoc: automatically insert docstrings from modules (y/n) [n]: (ENTER)
    > doctest: automatically test code snippets in doctest blocks (y/n) [n]: (ENTER)
    > intersphinx: link between Sphinx documentation of different projects (y/n) [n]: (ENTER)
    > todo: write "todo" entries that can be shown or hidden on build (y/n) [n]:
    > coverage: checks for documentation coverage (y/n) [n]: (ENTER)
    > imgmath: include math, rendered as PNG or SVG images (y/n) [n]: (ENTER)
    > mathjax: include math, rendered in the browser by MathJax (y/n) [n]: (ENTER)
    > ifconfig: conditional inclusion of content based on config values (y/n) [n]: (ENTER)
    > viewcode: include links to the source code of documented Python objects (y/n) [n]: (ENTER)
    > githubpages: create .nojekyll file to publish the document on GitHub pages (y/n) [n]: (ENTER)

    A Makefile and a Windows command file can be generated for you so that you
    only have to run e.g. `make html' instead of invoking sphinx-build
    directly.
    > Create Makefile? (y/n) [y]: (ENTER)
    > Create Windows command file? (y/n) [y]: (ENTER)

    Creating file ./source/conf.py.
    Creating file ./source/index.rst.
    Creating file ./Makefile.
    Creating file ./make.bat.

    Finished: An initial directory structure has been created.

.. note::
     Depending on the sphinx version installed, you may be asked with a shorter questionnaire.


Project tree
------------

The project tree will resemble like this if you choose to have separate source and build folders in the sphinx-quickstart questionnaire (recommended). Otherwise, the .rst files will be places in the main folder and the compilation output under a folder called _build.


.. code-block:: none
        
    ├── sphinx-project
        ├── Makefile
        ├── make.bat
        ├── build               <--- Compilation files created after first compilation.
        │   ├── doctrees
        │   ├── html            <--- Generated webpage.
        │   └── latex           <--- Generated .tex and .pdf in LaTeX.
        └── source              
            ├── conf.py         <--- Important file for configuration.
            ├── _static         <--- Empty folder by default. Add .gitkeep inside.
            ├── _templates      <--- Empty folder by default. Add .gitkeep inside.
            ├── _build          <--- VS-Code previsualization files.
            ├── .rst files      <--- Most important files.
            
.. note::
    Note that ``build`` does not exist just yet, but will be autogenerated later when we start compiling with ``make``.

- ``source``: This is where all our `.rst` files will reside. These are the files whose text will be converted to a desired format.
- ``source/conf.py``: This is the file where all Sphinx configuration settings (including the settings we specified during the ``sphinx-quickstart`` setup) are specified. When any ``make <builder>`` or ``sphinx-build <builder>`` command is called, Sphinx runs this file to extract the desired configuration.
- ``source/index.rst``: This is the file which tells Sphinx how to render our index.html page. In general, each ``source/*.rst`` file is converted to a corresponding ``build/html/*.html`` page when ``make html`` is called. **In order to be able to compile, the index.rst file must always contain the toctree.**
- `build` : This is the directory where the output of any builder is stored when a ``make <builder>`` or ``sphinx-build <builder>`` command is called. The generated web content will be stored in ``build/html``, whereas the .tex and .pdf files will be saved in ``build/latex``.


The conf.py file
----------------

This file is the Sphinx configuration builder. The options selected in the questionnaire after executing ``sphinx-quickstart`` are set here, and can be modified at convenience. Some options will appear commented (not applied), or will not be written at all. However, they can be added here if necessary. A complete list of all available options for the conf.py file can be found here:

https://www.sphinx-doc.org/en/master/usage/configuration.html

The most common modifications involve the html_theme and related options, the extensions to add features, and the autodoc configuration to point the script to the path where the target code is, in case we were interested in documenting software code.


.. _html_theme:

html_theme
^^^^^^^^^^

This line selects the html theme among those installed. For example, to enable the Read the Docs theme one has to set:

.. code-block:: none

    html_theme = 'sphinx_rtd_theme'

The personalization options via conf.py depends on the theme selected. For example, for the Read the Docs theme, a list of options is available here:

https://sphinx-rtd-theme.readthedocs.io/en/stable/configuring.html?highlight=html_theme_options#theme-options

For further personalization it is possible to tweak directly the .css file, and save it handle it as a custom theme.


.. _Extensions:

Extensions
^^^^^^^^^^

This is a field list that can be filled with the desired extensions that one want to activate, prior to their installation. For example:

.. code-block:: none

    extensions = [
    'sphinx.ext.autodoc',
    'sphinx_tabs.tabs',
    'sphinx_panels',
    ]


There exist a list of built-in extensions in :

https://www.sphinx-doc.org/en/master/usage/extensions/index.html

On the other hand, third-party extensions are found lurking on the internet :). There exist some lists available:

https://github.com/sphinx-contrib/

https://pypi.org/search/?c=Framework+%3A%3A+Sphinx+%3A%3A+Extension



Autodoc configuration (optional)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

In case you want to document modules/functions/code from source code (e.g. python), you will have to to enable ``autodoc`` during the initial questionnaire, when being asked:

.. code-block:: none

    autodoc: automatically insert docstrings from modules (y/n) [n]: y

If you didn't do it during the initial questionnaire, you can do it in the conf.py file adding it to extensions:

.. code-block:: none

    extensions = [
    'sphinx.ext.autodoc',
    ]

The next stop consist on uncommenting the following lines in the conf.py file:

.. code:: none

    import os
    import sys
    sys.path.insert(0, os.path.abspath('path/for/your/source/code/files'))

And replace in ``os.path.abspath`` with the path of your code files (or **relative path w.r.t. the folder containing conf.py**).

sphinx-apidoc and docstrings
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

If you have the Python Docstrings generator extension for VS-Code, just by typing inside a function \"\"\" tree times and pressing enter, the docstrings template to document that function will be generated. It should look something similar to this:

.. only:: html
    
    .. image:: images/docstring_demo.gif

.. only:: latex

    .. image:: images/docstring_demo.png

By documenting all your code in this way, the Sphinx generator will be able to create a formatted html or pdf from that. As a caveat, you just have to follow this rule:

.. warning::
    Make sure your Docstring (e.g. in Python) **is not outside a class, a method or a function.** It has to be self-contained. Sphinx when doing the autogeneration of the code, is going to run anything outside, and you may get weird compilation errors or warnings.

Once all your code is ready, go to the terminal and execute:

>>> sphinx-apidoc -o source_folder code_folder

Where source_folder is the folder with your .rst files, and the code folder is the folder where your e.g. Python code is located (it can ben inside the Sphinx project as well). Once you have executed this command, you should get a message indicating that some .rst files have been created under your source folder, namely the `modules.rst` plus more .rst files whose name matches the code files.

Finally, to include the code documentation in your html or pdf, add `modules` or `./modules.rst` to the toctree in your `index.rst`.


Writing .rst files
------------------

These files are written with reStructuredText syntax, which essentially is like writing a document in the wordpad and format it in ascii fashion. The most important rule to respect are tabbing to keep hierarchy of directives (like in python), and include the toctree (document index) in the index.rst file (or whatever is the name of your top file). Most causes of compilation errors or undesired formatting (e.g. a word or a sentence becomes bold for no reason) are due to:

- Incorrect tabbing :)

- Missing empty lines.

- Something wrong when calling a directive, i.e. incorrect use of symbols ` : _ 

Information about how to write in rst format and the use of directives (hierarchy with sections, figures, tables and so on...) can be found for example in:

https://www.sphinx-doc.org/en/master/usage/restructuredtext/directives.html

https://sphinx-rtd-theme.readthedocs.io/en/stable/demo/demo.html

However, the best way to learn is pressing the button :guilabel:`View page source` or :guilabel:`Edit on GitHub` that you will find in any webpage created with Sphinx (check both previous links). You can find other sites documented in this format, and if you see something fancy, check the code and copy-paste it. Some random examples:

https://sphinx-rtd-tutorial.readthedocs.io/

https://fsmmlk.github.io/inkscapeMadeEasy/

https://sphinx-tabs.readthedocs.io/

https://sphinx-panels.readthedocs.io/

https://pyvisa.readthedocs.io/

https://sphinx-charts.readthedocs.io/

https://mpb.readthedocs.io/


HTML compilation
----------------

To convert all your documentation in .rst format (plus possible source codes e.g. in python) into a fancy webpage, go to the main Sphinx project folder (the one containing the Makefile) and execute:

>>> make html

The html files for the webpage will be stored in the build ``build/html``. Inside this folder, open the ``index.html`` file with any web-browser.

If you want to specify the output target folder for your html files (useful if you want to keep the webpage as a separated git project), you can use:

>>> sphinx-build -b html source_folder output_folder


LaTeX compilation
------------------

Sphinx allows formatting in pdf as well, using LaTeX. To create the .tex file, you have to execute:

>>> make latex

This will place the .tex file into ``build/html``. The next step is compiling the .tex to create the pdf. You can either follow the typical route of compiling it directly or open the file with a LaTeX editor and compile from there (e.g. texstudio), or execute directly in the console:

>>> make latexpdf

And the pdf file will be saved in the ``build/latex`` folder as well. This method is preferred as some formatting problems have been observed if the .tex file is compiled using the typical method.

Bear in mind that there may be formatting differences between the html and the latex outputs. This is particularly true when adding custom extensions to the project. Check the results in both formats from time to time to ensure that you are getting what you expect.
