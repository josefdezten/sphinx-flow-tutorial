\babel@toc {english}{}
\contentsline {chapter}{\numberline {1}Prerequisites}{3}{chapter.1}%
\contentsline {section}{\numberline {1.1}Sphinx}{3}{section.1.1}%
\contentsline {subsection}{\numberline {1.1.1}Extensions for Sphinx}{3}{subsection.1.1.1}%
\contentsline {section}{\numberline {1.2}LaTeX}{4}{section.1.2}%
\contentsline {section}{\numberline {1.3}Git}{4}{section.1.3}%
\contentsline {section}{\numberline {1.4}VS\sphinxhyphen {}Code and useful plugins}{4}{section.1.4}%
\contentsline {chapter}{\numberline {2}Creating a Sphinx project}{7}{chapter.2}%
\contentsline {section}{\numberline {2.1}sphinx\sphinxhyphen {}quickstart}{7}{section.2.1}%
\contentsline {section}{\numberline {2.2}Project tree}{9}{section.2.2}%
\contentsline {section}{\numberline {2.3}The conf.py file}{9}{section.2.3}%
\contentsline {subsection}{\numberline {2.3.1}html\_theme}{10}{subsection.2.3.1}%
\contentsline {subsection}{\numberline {2.3.2}Extensions}{10}{subsection.2.3.2}%
\contentsline {subsection}{\numberline {2.3.3}Autodoc configuration (optional)}{10}{subsection.2.3.3}%
\contentsline {subsection}{\numberline {2.3.4}sphinx\sphinxhyphen {}apidoc and docstrings}{11}{subsection.2.3.4}%
\contentsline {section}{\numberline {2.4}Writing .rst files}{12}{section.2.4}%
\contentsline {section}{\numberline {2.5}HTML compilation}{12}{section.2.5}%
\contentsline {section}{\numberline {2.6}LaTeX compilation}{13}{section.2.6}%
\contentsline {chapter}{\numberline {3}Flow with Git}{15}{chapter.3}%
\contentsline {section}{\numberline {3.1}Preparing the Sphinx project}{15}{section.3.1}%
\contentsline {section}{\numberline {3.2}New Gitlab account (only first time)}{16}{section.3.2}%
\contentsline {section}{\numberline {3.3}Pushing an existing folder to a new Gitlab repository}{16}{section.3.3}%
\contentsline {section}{\numberline {3.4}Deploying the webpage with Gitlab Pages}{16}{section.3.4}%
\contentsline {subsection}{\numberline {3.4.1}As a separate project}{17}{subsection.3.4.1}%
\contentsline {subsection}{\numberline {3.4.2}All in one project}{19}{subsection.3.4.2}%
\contentsline {chapter}{\numberline {4}Indices and tables}{21}{chapter.4}%
